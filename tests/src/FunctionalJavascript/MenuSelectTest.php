<?php

namespace Drupal\Tests\menu_select\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;

/**
 * An end to end test of the menu select functionality.
 *
 * @group menu_select
 */
class MenuSelectTest extends WebDriverTestBase {

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'menu_select',
  ];

  /**
   * Test the menu select module.
   */
  public function testMenuSelect() {
    $menu = Menu::create([
      'id' => 'menu',
      'label' => 'Menu',
      'description' => 'Menu',
    ]);
    $menu->save();

    // @codingStandardsIgnoreStart
    $this->transformTreeIntoPagesAndMenuItems([
      'foo' => [
        'bar',
        'baz',
      ],
      'dip' => [
        'dap',
        'sip' => [
          'lip',
          'blam' => [
            'wham',
          ],
          'flam',
        ],
      ],
      'wap' => [],
    ]);
    // @codingStandardsIgnoreEnd

    $this->drupalLogin($this->createUser([
      'administer menu',
    ]));

    $this->drupalGet('admin/structure/menu/manage/menu/add');

    // Click through the tree of items.
    $this->clickLink('Main navigation (menu)');
    $this->clickLink('foo');
    $this->clickLink('bar');

    // Save the menu link.
    $this->submitForm([
      'title[0][value]' => 'new sample link',
      'link[0][uri]' => '<front>',
    ], 'Save');
    $this->assertSession()->pageTextContains('The menu link has been saved.');

    // Ensure when the page reloads, the selected menu link has been reselected.
    $this->assertSession()->elementContains('css', '.menu-select-menu-link.active', 'bar');
  }

  /**
   * Transform a tree into test pages and menu links.
   */
  protected function transformTreeIntoPagesAndMenuItems($tree, $parent = NULL, $menu = 'main') {
    foreach ($tree as $key => $tree_item) {
      if (is_array($tree_item)) {
        $new_parent = $this->createLink($key, $parent, $menu);
        $this->transformTreeIntoPagesAndMenuItems($tree_item, $new_parent, $menu);
      }
      else {
        $this->createLink($tree_item, $parent, $menu);
      }
    }
  }

  /**
   * Create a link.
   */
  private function createLink($name, $parent, $menu) {
    static $weight;
    if (isset($weight)) {
      $weight = 1;
    }
    else {
      $weight = 0;
    }
    $link = MenuLinkContent::create([
      'link' => [['uri' => 'route:<front>']],
      'title' => $name,
      'menu_name' => $menu,
      'weight' => $weight,
      'parent' => $parent ? $parent->getPluginId() : NULL,
      'expanded' => TRUE,
    ]);
    $link->save();
    return $link;
  }

}
