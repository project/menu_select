<?php

namespace Drupal\menu_select\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a controller class with methods for auto complete.
 */
class MenuSelectAutocompleteController extends ControllerBase {

  /**
   * The tree builder service.
   *
   * @var \Drupal\menu_select\MenuSelectTreeBuilderInterface
   */
  protected $treeBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->treeBuilder = $container->get('menu_select.tree_builder');
    return $instance;
  }

  /**
   * Returns autocomplete content for the given search queries.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   */
  public function autocomplete($menus, $max_depth, Request $request) {
    $keyword = $request->query->get('q');
    $menu_ids = explode(':', $menus);

    $matching_links = $this->getMatchingLinks($keyword, $menu_ids, $max_depth);

    $autocomplete = [];
    foreach ($matching_links as $key => $label) {
      $autocomplete[] = ['value' => (string) $key, 'label' => $label];
    }

    return new JsonResponse($autocomplete);
  }

  /**
   * Get links matching the given keyword.
   *
   * @param string $keyword
   *   The keyword to search for.
   * @param array $menus
   *   An array of menus to search.
   * @param int $max_depth
   *   The maximum depth to search.
   *
   * @return array
   *   An array of link options matching the keyword.
   */
  protected function getMatchingLinks($keyword, array $menus, $max_depth) {
    $options = [];
    foreach ($menus as $menu_name) {
      $tree = $this->treeBuilder->loadMenuTree($menu_name, $max_depth);
      $candidates = [];
      $this->buildCandidateLinks($tree, $menu_name, $candidates);
      foreach ($candidates as $key => $menu_link_label) {
        if (stripos($menu_link_label, $keyword) !== FALSE) {
          $options[$key] = $menu_link_label;
        }
      }
    }
    return $options;
  }

  /**
   * Build a recursive list of all candidates.
   *
   * @param array $tree
   *   A menu tree.
   * @param string $menu_name
   *   The menu name.
   * @param array $options
   *   An array of options, built recurwsively.
   */
  protected function buildCandidateLinks(array $tree, $menu_name, array &$options) {
    foreach ($tree as $data) {
      $title = $data->link->getTitle();
      $options[$menu_name . ':' . $data->link->getPluginId()] = $title;
      if (!empty($data->subtree)) {
        $this->buildCandidateLinks($data->subtree, $menu_name, $options);
      }
    }
  }

}
