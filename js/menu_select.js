(function (Drupal, $, once) {
  Drupal.behaviors.menu_select = {
    attach: function (context, settings) {
      $(once('menu-select-tree', '.js-menu-select-tree', context)).each(function () {
        var $wrapper = $(this);
        var $links = $wrapper.find('.js-menu-select-menu-link');
        var $input = $wrapper.find('.js-menu-select-tree-parent-id');
        var $search = $wrapper.find('.js-menu-select-parent-menu-item-search');
        var $preview = $wrapper.find('.js-menu-select-parent-position-preview');

        // Perform initial setup on page load.
        Drupal.behaviors.menu_select.setUpMenuSelect($wrapper, $links, $input, $preview, $input.val());

        $links.on('click', function (e) {
          // Prevent the link from changing the page.
          e.preventDefault();

          // Get the clicked link item.
          var $link = $(this);

          // Remove active style form all links.
          $links.removeClass('active');

          // Update the select box.
          $input.val($link.data('mkey'));

          // Update the position preview.
          Drupal.behaviors.menu_select.updatePreview($link, $preview);

          // Add active style to clicked link.
          // Expand the link if need be.
          $link.toggleClass('active');
          if ($link.next().hasClass('item-list')) {
            $link.toggleClass('expanded');
            $link.next().toggleClass('expanded');
          }
        });

        $search.on('autocompleteclose', (function (e) {
          // The menu link key or the search string.
          var mKey = $(this).val();

          // Check whether link with given key is found.
          if ($links.filter('[data-mkey="' + mKey + '"]').length > 0) {
            // Remove active and expanded styling from all links.
            $links.removeClass('active');
            $wrapper.find('.expanded').removeClass('expanded');

            // Clear the field once we have the key.
            $search.val('');

            // Update the menu with chosen link.
            Drupal.behaviors.menu_select.updateChosenOne($links, $input, $preview, mKey);
          }
        }));

      });
    },

    /**
     * Called on setup when the component is loaded.
     */
    setUpMenuSelect: function ($wrapper, $links, $input, $preview, mKey) {
      // Remove active and expanded styling from all links.
      $links.removeClass('active');
      $wrapper.find('.expanded').removeClass('expanded');

      // Set up links and children.
      $links.each(function () {

        // Determine if this link has any children and add a class.
        var $link = $(this);
        if ($link.next().hasClass('item-list')) {
          $link.addClass('menu-select-menu-link--has-children')
        }

        // Add some markup to enable additional styling.
        $link.html($link.html() + '<span></span>');

        // Set up the position preview.
        $preview.html(Drupal.t('Select a menu item.'));
      });

      // And now build based on the content's current menu parent.
      Drupal.behaviors.menu_select.updateChosenOne($links, $input, $preview, mKey);
    },

    /**
     * Called when the chosen menu item is updated.
     */
    updateChosenOne: function ($links, $input, $preview, mKey) {
      // Find the chosen link.
      var $selectedLink = $links.filter('[data-mkey="' + mKey + '"]');

      // Update the select box with the chosen link.
      $input.val(mKey);

      // Add active style to the chosen link.
      $selectedLink.addClass('active');

      // Update the position preview.
      Drupal.behaviors.menu_select.updatePreview($selectedLink, $preview);

      // Run through and expand parents.
      var $parents = $selectedLink.parents('.js-menu-select-tree li');
      $parents.each(function () {
        $(this).find('> a, > .item-list').addClass('expanded');
      });
    },

    /**
     * Update the preview element.
     */
    updatePreview: function ($link, $preview) {
      $preview.html('');
      // By running back up through the clicked item's parents.
      var previewVal = '';
      var parents = $link.parents('.js-menu-select-tree li').get().reverse();
      $(parents).each(function () {
        previewVal += $(this).children('a').html() + ' > ';
      });
      $preview.html(previewVal + '<strong>' + Drupal.t('New item') + '</strong>');
    }
  }
})(Drupal, jQuery, once);
