<?php

namespace Drupal\menu_select\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * A form element for the select tree.
 *
 * @FormElement("menu_select_tree")
 */
class MenuSelectTree extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#menu_parent' => NULL,
      '#default_value' => NULL,
      '#value' => NULL,
      '#options' => [],
      '#current_link_id' => NULL,
      '#max_depth' => NULL,
      '#process' => [
        [static::class, 'processElement'],
      ],
      '#element_validate' => [
        [static::class, 'validateMenuSelectTree'],
      ],
      '#attached' => [
        'library' => ['menu_select/menu_select'],
      ],
      '#tree' => TRUE,
      'tree' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['js-menu-select-tree'],
        ],
        'menu_parent_id' => [
          '#type' => 'hidden',
          '#attributes' => [
            'class' => ['js-menu-select-tree-parent-id'],
          ],
        ],
      ],
    ];
  }

  /**
   * Process the element.
   */
  public static function processElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['tree']['menu_parent_id']['#default_value'] = $element['#value'] = $element['#default_value'];

    $element['tree']['parent_menu_position_preview'] = [
      '#type' => 'item',
      '#title' => t('Menu link position preview'),
      '#description' => t("Preview of the menu item's hierarchical position."),
    ];
    $element['tree']['parent_menu_position_preview']['children'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'menu-select-parent-position-preview',
          'js-menu-select-parent-position-preview',
        ],
      ],
    ];

    $config = \Drupal::config('menu_select.settings');
    $search_enabled = $config->get('search_enabled');
    if ($search_enabled && \Drupal::currentUser()->hasPermission('use menu select search')) {
      $element['tree']['parent_menu_item_search'] = [
        '#type' => 'textfield',
        '#title' => t('Search'),
        '#autocomplete_route_name' => 'menu_select.menu_select_autocomplete',
        '#autocomplete_route_parameters' => [
          'menus' => implode(':', array_keys($element['#options'])),
          'max_depth' => $element['#max_depth'],
        ],
        '#description' => t("Alternatively, use this autocomplete search to find the menu item's parent and select it."),
        '#attributes' => [
          'class' => [
            'menu-select-parent-menu-item-search',
            'js-menu-select-parent-menu-item-search',
          ],
        ],
      ];
    }

    /** @var \Drupal\menu_select\MenuSelectTreeBuilder $tree_builder */
    $tree_builder = \Drupal::service('menu_select.tree_builder');
    $element['tree']['parent_menu_item'] = [];
    foreach ($element['#options'] as $menu_machine_name => $menu_label) {
      $menu_tree = $tree_builder->loadMenuTree($menu_machine_name, $element['#max_depth']);
      $element['tree']['parent_menu_item'][] = $tree_builder->buildRenderedMenu($menu_tree, $menu_machine_name, $menu_label, $element['#current_link_id']);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return $input ? $input['tree']['menu_parent_id'] : FALSE;
  }

  /**
   * Validate the element.
   *
   * Setting the form state value, in addition to ::valueCallback is required
   * for this element to return a single string, instead of an array.
   */
  public static function validateMenuSelectTree(&$element, FormStateInterface $form_state, &$complete_form) {
    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
    if ($input_exists) {
      $form_state->setValueForElement($element, $input['tree']['menu_parent_id']);
    }
  }

}
