<?php

namespace Drupal\menu_select;

/**
 * An interface for the menu select tree builder service.
 */
interface MenuSelectTreeBuilderInterface {

  /**
   * Function to build a menu tree from a menu name.
   *
   * @param string $menu_name
   *   Menu name to use.
   * @param int $max_depth
   *   The maximum depth to traverse.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   Array of the menu tree.
   */
  public function loadMenuTree($menu_name, $max_depth);

  /**
   * Builds a renderable array of the given menu tree.
   *
   * @param array $menu_tree
   *   The menu tree.
   * @param string $menu_id
   *   The machine name of the menu being built.
   * @param string $menu_label
   *   The label of the menu being built.
   * @param string $current_link_id
   *   The current link plugin ID, used for exclusion.
   *
   * @return array
   *   A renderable array.
   */
  public function buildRenderedMenu(array $menu_tree, $menu_id, $menu_label, $current_link_id);

}
