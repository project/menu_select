<?php

namespace Drupal\menu_select;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuParentFormSelector;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Decorates the core 'menu.parent_form_selector' service.
 */
class MenuSelectParentFormSelector extends MenuParentFormSelector {

  /**
   * The inner service from decorator.
   *
   * @var \Drupal\Core\Menu\MenuParentFormSelectorInterface
   */
  protected $inner;

  /**
   * Constructs a \Drupal\menu_select\MenuSelectParentFormSelector.
   *
   * @param \Drupal\Core\Menu\MenuParentFormSelectorInterface $inner
   *   The inner service from decorator.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   The menu link tree service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(MenuParentFormSelectorInterface $inner, MenuLinkTreeInterface $menu_link_tree, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    parent::__construct($menu_link_tree, $entity_type_manager, $string_translation);
    $this->inner = $inner;
  }

  /**
   * {@inheritdoc}
   */
  public function parentSelectElement($menu_parent, $id = '', array $menus = NULL) {
    // Run the inner method, in case inner services execute some important code.
    $element = $this->inner->parentSelectElement($menu_parent, $id, $menus);
    // Instead of building out a select list, return the custom menu select tree
    // element.
    $element['#type'] = 'menu_select_tree';
    $element['#default_value'] = $menu_parent;
    $element['#menu_parent'] = $menu_parent;
    $element['#options'] = !isset($menus) ? $this->inner->getMenuOptions($menus) : $menus;
    $element['#max_depth'] = $this->inner->getParentDepthLimit($id);
    $element['#current_link_id'] = $id;
    return $element;
  }

}
